FROM rust:latest

WORKDIR /ci/docker/rust
COPY . .

RUN rustup install stable
RUN rustup install beta
RUN rustup install nightly

RUN rustup component add rustfmt --toolchain stable
RUN rustup component add rustfmt --toolchain beta
RUN rustup component add rustfmt --toolchain nightly

RUN rustup component add clippy --toolchain stable
RUN rustup component add clippy --toolchain beta

RUN export DEBIAN_FRONTEND= noninteractive
RUN apt-get update -y
RUN apt-get install -y zip unzip
RUN cargo install grcov
