[![crates-url](https://docs.rs/kitty_pool/badge.svg)](https://docs.rs/kitty_pool/0.3.0/kitty_pool/)
[![codecov](https://codecov.io/gl/nyanzebra/kitty_pool/branch/develop/graph/badge.svg)](https://codecov.io/gl/nyanzebra/kitty_pool)
[![pipeline status](https://gitlab.com/nyanzebra/kitty_pool/badges/develop/pipeline.svg)](https://gitlab.com/nyanzebra/kitty_pool/commits/develop)

# Kitty Pool
## Purpose
The goal of this crate is to provide a safe and easy to use buffer pool in both a single-threaded and multi-threaded environment. The 'Kitty Pool' allows for requesting of 'Ranges', which act as owned slices into the buffer. The user of the 'Ranges' can then safely read and write to the owned parts of the buffer. The 'Kitty Pool' also allows for a standard scatter gather list implementation.
## Example
```rust
use kitty_pool::*;

const POOL_SIZE: usize = 1024;
const BLOCK_SIZE: usize = 64;
const REQUEST_SIZE: usize = 256;

fn main() {
  // Supports Contiguous and ScatterGatherList
  let result = kitty_pool(KittyPoolOrganization::Contiguous, POOL_SIZE, BLOCK_SIZE);
  assert!(result.is_ok());
  let mut kitty_pool = result.unwrap();
  let rand_string: String = thread_rng()
      .sample_iter(&Alphanumeric)
      .take(REQUEST_SIZE)
      .collect();
  let rand_bytes: Vec<u8> = Vec::from(rand_string.as_bytes());
  let mut buffer = vec![0; REQUEST_SIZE];
  let future = async {
      let mut token = kitty_pool.borrow(REQUEST_SIZE).await;
      assert!(token.is_ok());
      token = kitty_pool.write(&token.unwrap(), &rand_bytes).await;
      assert!(token.is_ok());
      token = kitty_pool.read(&token.unwrap(), &mut buffer).await;
      assert!(token.is_ok());
      assert_eq!(*buffer, *rand_bytes);
      assert!(kitty_pool.release(&token.unwrap()).is_ok());
  };
  block_on(future);
}
```
