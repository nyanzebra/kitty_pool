#![cfg_attr(feature = "unstable", feature(test))]

mod buffer;
pub mod pool;

#[cfg(all(feature = "unstable", test))]
mod bench {
    use super::*;
    use kitty_pool::{BorrowBufferResponse, KittyPool};
    use rand::{distributions::Alphanumeric, thread_rng, Rng};
    use std::{
        io::{Read, Write},
        sync::Arc,
        thread,
    };
    use test::Bencher;
    use tokio::prelude::{future::ok, Future};

    const POOL_BLOCKS: usize = 64;
    const POOL_SIZE: usize = 1024 * 1024 * 64;
    const REQUEST_SIZE: usize = 1024 * 1024 * 16;
    const REQUESTS: usize = 10;

    fn test_fn(pool: Arc<KittyPool>) {
        let rand_string: String = thread_rng()
            .sample_iter(&Alphanumeric)
            .take(REQUEST_SIZE)
            .collect();

        let write_buffer = rand_string.into_bytes();
        let mut read_buffer = vec![0; REQUEST_SIZE];

        let future = async {
            let mut token = kitty_pool.borrow(REQUEST_SIZE).await;
            assert!(token.is_ok());
            token = kitty_pool.write(&token.unwrap(), &rand_bytes).await;
            assert!(token.is_ok());
            token = kitty_pool.read(&token.unwrap(), &mut buffer).await;
            assert!(token.is_ok());
            assert_eq!(*buffer, *rand_bytes);
            assert!(kitty_pool.release(&token.unwrap()).is_ok());
        };
        block_on(future);
    }

    #[bench]
    fn bench_thread_single_context(b: &mut Bencher) {
        let mut kitty_pool = Arc::from(KittyPool::new(POOL_SIZE, POOL_BLOCKS));

        b.iter(|| {
            let kitty_clone = kitty_pool.clone();
            test_fn(kitty_clone);
        })
    }

    #[bench]
    fn bench_thread_multiple_contexts(b: &mut Bencher) {
        let mut kitty_pool = Arc::from(KittyPool::new(POOL_SIZE, POOL_BLOCKS));

        b.iter(|| {
            let mut handles = Vec::new();
            for _ in 0..REQUESTS {
                let kitty_clone = kitty_pool.clone();
                handles.push(thread::spawn(move || test_fn(kitty_clone)));
            }

            for handle in handles.into_iter() {
                assert!(handle.join().is_ok());
            }
        })
    }
}
