use std::{
    cmp::min,
    io::{Error, ErrorKind, Read, Seek, SeekFrom, Write},
    ptr::copy_nonoverlapping,
};

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Buffer {
    inner: Vec<u8>,
    sentinel: usize,
    capacity: usize,
}

impl Read for Buffer {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, Error> {
        let buffer = &self.inner;
        let len = min(buf.len(), buffer.len());
        unsafe {
            copy_nonoverlapping(buffer[self.sentinel..].as_ptr(), buf.as_mut_ptr(), len);
        }
        Ok(len)
    }
}

impl Write for Buffer {
    fn write(&mut self, buf: &[u8]) -> Result<usize, Error> {
        let buffer = &mut self.inner;

        let len = min(buf.len(), buffer.len());
        unsafe {
            copy_nonoverlapping(buf.as_ptr(), buffer[self.sentinel..].as_mut_ptr(), len);
        }
        Ok(len)
    }

    fn flush(&mut self) -> Result<(), Error> {
        Ok(())
    }
}

impl Seek for Buffer {
    fn seek(&mut self, pos: SeekFrom) -> Result<u64, Error> {
        match pos {
            SeekFrom::Start(start) => {
                if (start as usize) < self.capacity {
                    self.sentinel = start as usize;
                    return Ok(start);
                }
                Err(Error::new(
                    ErrorKind::InvalidInput,
                    format!(
                        "Impossible to seek to {} as it is out of bounds {}",
                        start, self.capacity
                    ),
                ))
            }
            SeekFrom::End(end) => {
                if end + self.capacity as i64 > -1 && end < 0 {
                    self.sentinel = self.capacity - end.abs() as usize;
                    return Ok(self.sentinel as u64);
                }
                Err(Error::new(
                    ErrorKind::InvalidInput,
                    format!(
                        "Impossible to seek to {} as it is out of bounds {}",
                        end, self.capacity
                    ),
                ))
            }
            SeekFrom::Current(current) => {
                let position = (self.sentinel as i64) + current;
                if position < self.capacity as i64 && position > 0 {
                    if current < 0 {
                        self.sentinel -= current.abs() as usize;
                    } else {
                        self.sentinel += current as usize;
                    };
                    return Ok(self.sentinel as u64);
                }
                Err(Error::new(
                    ErrorKind::InvalidInput,
                    format!(
                        "Impossible to seek to {} as it is out of bounds {}",
                        current, self.capacity
                    ),
                ))
            }
        }
    }
}

impl Buffer {
    pub fn new(capacity: usize) -> Self {
        Self {
            inner: vec![0; capacity],
            sentinel: 0,
            capacity,
        }
    }

    pub fn as_slice(&self) -> &[u8] {
        self.inner.as_slice()
    }

    pub fn len(&self) -> usize {
        self.capacity
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new() {
        for i in 0..1000 {
            let buffer = Buffer::new(i);
            assert_eq!(buffer.len(), i);
        }
    }

    #[test]
    fn test_clone() {
        let buffer = Buffer::new(1);
        let clone = buffer.clone();

        assert_eq!(buffer.len(), clone.len());
        assert_eq!(buffer.as_slice(), clone.as_slice());
    }

    #[test]
    fn test_eq() {
        let buffer = Buffer::new(1);
        let clone = buffer.clone();

        assert_eq!(buffer, clone);

        let other = Buffer::new(2);

        assert_ne!(buffer, other);
    }

    #[test]
    fn test_debug() {
        let buffer = Buffer::new(1);
        assert_eq!(
            &format!("{:?}", buffer),
            "Buffer { inner: [0], sentinel: 0, capacity: 1 }"
        );
    }

    #[test]
    fn test_as_slice() {
        let mut buffer = Buffer::new(1);
        assert_eq!(buffer.as_slice(), &[0]);
        let result = buffer.write(&[1]);
        assert!(result.is_ok());
        let flush = buffer.flush();
        assert!(flush.is_ok());
        assert_eq!(buffer.as_slice(), &[1]);
    }

    #[test]
    fn test_read_write() {
        let mut buffer = Buffer::new(100);
        {
            let result = buffer.write(&[1; 10]);
            assert!(result.is_ok());
            let flush = buffer.flush();
            assert!(flush.is_ok());
            assert_eq!(result.unwrap(), 10);
        }
        {
            let mut dst: [u8; 10] = [0; 10];
            let result = buffer.read(&mut dst);
            assert!(result.is_ok());
            let flush = buffer.flush();
            assert!(flush.is_ok());
            assert_eq!(dst, [1; 10]);
        }
    }

    #[test]
    fn test_flush() {
        let mut buffer = Buffer::new(100);
        assert!(buffer.flush().is_ok());
    }

    #[test]
    fn test_seek() {
        let mut buffer = Buffer::new(100);
        {
            let result = buffer.seek(SeekFrom::Start(10));
            assert!(result.is_ok());
            assert_eq!(result.unwrap(), 10);
        }
        {
            let result = buffer.seek(SeekFrom::End(-1));
            assert!(result.is_ok());
            assert_eq!(result.unwrap(), 99);
        }
        {
            let result = buffer.seek(SeekFrom::Current(-1));
            assert!(result.is_ok());
            assert_eq!(result.unwrap(), 98);
        }
        {
            let result = buffer.seek(SeekFrom::Current(1));
            assert!(result.is_ok());
            assert_eq!(result.unwrap(), 99);
        }
        {
            let result = buffer.seek(SeekFrom::Current(1));
            assert!(result.is_err());
        }
        {
            let result = buffer.seek(SeekFrom::End(100));
            assert!(result.is_err());
        }
        {
            let result = buffer.seek(SeekFrom::Start(100));
            assert!(result.is_err());
        }
    }
}
