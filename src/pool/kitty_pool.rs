use crate::pool::error::Error;
use async_trait::async_trait;
use std::fmt::Debug;
use uuid::Uuid;

pub type Token = Uuid;

#[async_trait]
pub trait KittyPool: Debug {
    async fn borrow(&mut self, mut requested_size: usize) -> Result<Token, Error>;

    fn release(&mut self, token: &Token) -> Result<(), Error>;

    async fn read(&mut self, token: &Token, data_to_read: &mut [u8]) -> Result<Token, Error>;

    async fn write(&mut self, token: &Token, data_to_write: &[u8]) -> Result<Token, Error>;
}
