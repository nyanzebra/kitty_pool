use std::{
    fmt::{Display, Formatter, Result},
    io::{Error as IoError, ErrorKind},
};

#[derive(Debug)]
pub struct Error {
    message: String,
    kind: Option<ErrorKind>,
}

impl Error {
    pub fn new(message: String) -> Self {
        Self {
            message,
            kind: None,
        }
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        write!(f, "Error: {}", &self.message)
    }
}

impl From<IoError> for Error {
    fn from(error: IoError) -> Self {
        Self {
            message: format!("{}", error),
            kind: Some(error.kind()),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new() {
        let error = Error::new("Test".to_string());
        assert_eq!(error.to_string(), "Error: Test");
        assert_eq!(error.kind, None);
    }

    #[test]
    fn test_debug() {
        let error = Error::new("Test".to_string());
        assert_eq!(
            &format!("{:?}", error),
            "Error { message: \"Test\", kind: None }"
        );
    }

    #[test]
    fn test_display() {
        let error = Error::new("Test".to_string());
        assert_eq!(&format!("{}", error), "Error: Test");
    }

    #[test]
    fn test_from() {
        let error = Error::from(IoError::new(ErrorKind::Other, "Test"));
        assert_eq!(error.to_string(), "Error: Test");
        assert_eq!(error.kind, Some(ErrorKind::Other));
    }
}
