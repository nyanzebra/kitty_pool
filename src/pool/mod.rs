// The pool provides an interface to a large Buffer
// The pool will give back ranges that can be used for reading or writing

use crate::pool::{
    contiguous::ContiguousKittyPool, error::Error, kitty_pool::KittyPool, sgl::SGLKittyPool,
};

pub mod contiguous;
pub mod error;
pub mod kitty_pool;
pub mod sgl;

pub enum KittyPoolOrganization {
    Contiguous,
    ScatterGatherList,
}

pub fn kitty_pool(
    kitty_pool_organization: KittyPoolOrganization,
    capacity: usize,
    block_size: usize,
) -> Result<Box<dyn KittyPool>, Error> {
    if capacity % block_size != 0 {
        return Err(Error::new(format!(
            "Cannot construct pool as block size {} does not go into {}",
            block_size, capacity
        )));
    }
    Ok(match kitty_pool_organization {
        KittyPoolOrganization::Contiguous => {
            Box::new(ContiguousKittyPool::new(capacity, block_size))
        }
        KittyPoolOrganization::ScatterGatherList => {
            Box::new(SGLKittyPool::new(capacity, block_size))
        }
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    use crate::pool::KittyPoolOrganization;
    use futures::executor::block_on;
    use rand::{distributions::Alphanumeric, thread_rng, Rng};

    const POOL_SIZE: usize = 1024;
    const BLOCK_SIZE: usize = 64;
    const REQUEST_SIZE: usize = 256;

    #[test]
    fn test_con() {
        {
            let result = kitty_pool(KittyPoolOrganization::Contiguous, POOL_SIZE, BLOCK_SIZE + 1);
            assert!(result.is_err());
            assert_eq!(
                format!("{:?}", result.unwrap_err()),
                format!(
                    "Error {{ message: \"Cannot construct pool as block size {} does not go into {}\", kind: None }}",
                    BLOCK_SIZE + 1,
                    POOL_SIZE
                )
            );
        }
        {
            let result = kitty_pool(KittyPoolOrganization::Contiguous, POOL_SIZE, BLOCK_SIZE);
            assert!(result.is_ok());
            let mut kitty_pool = result.unwrap();
            let rand_string: String = thread_rng()
                .sample_iter(&Alphanumeric)
                .take(REQUEST_SIZE)
                .collect();
            let rand_bytes: Vec<u8> = Vec::from(rand_string.as_bytes());
            let mut buffer = vec![0; REQUEST_SIZE];
            let future = async {
                let mut token = kitty_pool.borrow(REQUEST_SIZE).await;
                assert!(token.is_ok());
                token = kitty_pool.write(&token.unwrap(), &rand_bytes).await;
                assert!(token.is_ok());
                token = kitty_pool.read(&token.unwrap(), &mut buffer).await;
                assert!(token.is_ok());
                assert_eq!(*buffer, *rand_bytes);
                assert!(kitty_pool.release(&token.unwrap()).is_ok());
            };
            block_on(future);
        }
    }

    #[test]
    fn test_sgl() {
        {
            let result = kitty_pool(
                KittyPoolOrganization::ScatterGatherList,
                POOL_SIZE,
                BLOCK_SIZE + 1,
            );
            assert!(result.is_err());
            assert_eq!(
                format!("{:?}", result.unwrap_err()),
                format!(
                    "Error {{ message: \"Cannot construct pool as block size {} does not go into {}\", kind: None }}",
                    BLOCK_SIZE + 1,
                    POOL_SIZE
                )
            );
        }
        {
            let result = kitty_pool(
                KittyPoolOrganization::ScatterGatherList,
                POOL_SIZE,
                BLOCK_SIZE,
            );
            assert!(result.is_ok());
            let mut kitty_pool = result.unwrap();
            let rand_string: String = thread_rng()
                .sample_iter(&Alphanumeric)
                .take(REQUEST_SIZE)
                .collect();
            let rand_bytes: Vec<u8> = Vec::from(rand_string.as_bytes());
            let mut buffer = vec![0; REQUEST_SIZE];
            let future = async {
                let mut token = kitty_pool.borrow(REQUEST_SIZE).await;
                assert!(token.is_ok());
                token = kitty_pool.write(&token.unwrap(), &rand_bytes).await;
                assert!(token.is_ok());
                token = kitty_pool.read(&token.unwrap(), &mut buffer).await;
                assert!(token.is_ok());
                assert_eq!(*buffer, *rand_bytes);
                assert!(kitty_pool.release(&token.unwrap()).is_ok());
            };
            block_on(future);
        }
    }
}
