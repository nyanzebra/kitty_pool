export CARGO_INCREMENTAL=0
export CODECOV_TOKEN="73621dcd-8403-4b0d-bbba-b17e86b58d88"
export RUSTFLAGS="-Zprofile -Ccodegen-units=1 -Cinline-threshold=0 -Clink-dead-code -Coverflow-checks=off -Zno-landing-pads"
cargo clean
cargo +nightly build
cargo +nightly test
zip -0 ccov.zip `find . \( -name "kitty_pool*.gc*" \) -print`
grcov ccov.zip -s . -t lcov --llvm --branch --ignore-not-existing --ignore "/*" -o lcov.info
genhtml -o report/ --show-details --highlight --ignore-errors source --legend lcov.info
export CARGO_INCREMENTAL=
export CODECOV_TOKEN=
export RUSTFLAGS=
